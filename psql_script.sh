#!/bin/bash

# Step 1: Create local and foreign databases and users
echo "Creating local and foreign databases and users..."
psql -c "CREATE DATABASE localdb;"
psql -c "CREATE DATABASE foreigndb;"
psql -c "CREATE USER localuser WITH PASSWORD 'localpassword';"
psql -c "CREATE USER fdwUser WITH PASSWORD 'secret';"


# Step 1.5: Update pg_hba.conf for foreign database
echo "Updating pg_hba.conf for foreign database..."
PG_HBA_CONF_FOREIGN=$(psql -d foreigndb -c "SHOW hba_file;" | tail -3 | head -1)
echo "host    foreigndb       fdwUser         127.0.0.1/32            md5" >> $PG_HBA_CONF_FOREIGN

# Step 2: Create the Extension in localdb
echo "Creating extension in localdb..."
psql -d localdb -c "CREATE EXTENSION IF NOT EXISTS postgres_fdw;"

# Step 3: Create the Foreign Server in localdb
echo "Creating foreign server in localdb..."
psql -d localdb -c "CREATE SERVER foreigndb_fdw FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '127.0.0.1', port '5432', dbname 'foreigndb');"

# Step 4: Create User Mapping in localdb
echo "Creating user mapping in localdb..."
psql -d localdb -c "CREATE USER MAPPING FOR localuser SERVER foreigndb_fdw OPTIONS (user 'fdwUser', password 'secret');"

# Step 5: Grant Access to Local User in localdb
echo "Granting access to local user in localdb..."
psql -d localdb -c "GRANT USAGE ON FOREIGN SERVER foreigndb_fdw TO localuser;"

# Step 6: Import Foreign Schema or Tables in localdb
echo "Importing foreign schema in localdb..."
psql -U localuser -d localdb -c "IMPORT FOREIGN SCHEMA public LIMIT TO (account_metrics) FROM SERVER foreigndb_fdw INTO public;"

echo "Setup complete!"
