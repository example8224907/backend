FROM golang:1.21

WORKDIR /app

COPY go.mod ./

COPY main.go ./

RUN go mod download

RUN go build -o main

EXPOSE 8090

CMD ["go", "run", "main.go"]
